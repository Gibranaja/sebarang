<?php

    require "../config/connection.php";

    Class FasilitasKamar
    {
        public function __construct()
        {}
        public function get_data()
        {
            $sql = "SELECT * FROM tb_fasilitas";
            return runQuery($sql);
        }

        public function insert($tipe_kamar, $fasilitas_kamar)
        {
            $sql = "INSERT INTO tb_fasilitas (tipe_kamar, fasilitas_kamar) VALUES ('$tipe_kamar', '$fasilitas_kamar')";
            return runQuery($sql);
        }

        public function update($id_kamar, $tipe_kamar, $fasilitas_kamar)
        {
            $sql = "UPDATE tb_fasilitas SET tipe_kamar='$tipe_kamar', fasilitas_kamar='$fasilitas_kamar' WHERE id_kamar = '$id_kamar'";
            return runQuery($sql);
        }
        
        public function show($id_kamar)
        {
            $sql = "SELECT * FROM tb_fasilitas WHERE id_kamar='$id_kamar'";
            return runQueryRow($sql);
        }

        public function delete_data($id_kamar)
        {
            $sql = "DELETE FROM tb_fasilitas WHERE id_kamar='$id_kamar'";
            return runQuery($sql);
        }
    }